################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/user/TFT/gui.c \
../Core/user/TFT/lcd.c \
../Core/user/TFT/my_spi.c \
../Core/user/TFT/test.c 

OBJS += \
./Core/user/TFT/gui.o \
./Core/user/TFT/lcd.o \
./Core/user/TFT/my_spi.o \
./Core/user/TFT/test.o 

C_DEPS += \
./Core/user/TFT/gui.d \
./Core/user/TFT/lcd.d \
./Core/user/TFT/my_spi.d \
./Core/user/TFT/test.d 


# Each subdirectory must supply rules for building sources it contributes
Core/user/TFT/gui.o: ../Core/user/TFT/gui.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DSTM32F103xE -DUSE_HAL_DRIVER -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"D:/project/Personal_projects/heat_gun/firmware/demo/Core/user/TFT" -I"D:/project/Personal_projects/heat_gun/firmware/demo/Core/user/AT24C02" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/user/TFT/gui.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Core/user/TFT/lcd.o: ../Core/user/TFT/lcd.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DSTM32F103xE -DUSE_HAL_DRIVER -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"D:/project/Personal_projects/heat_gun/firmware/demo/Core/user/TFT" -I"D:/project/Personal_projects/heat_gun/firmware/demo/Core/user/AT24C02" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/user/TFT/lcd.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Core/user/TFT/my_spi.o: ../Core/user/TFT/my_spi.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DSTM32F103xE -DUSE_HAL_DRIVER -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"D:/project/Personal_projects/heat_gun/firmware/demo/Core/user/TFT" -I"D:/project/Personal_projects/heat_gun/firmware/demo/Core/user/AT24C02" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/user/TFT/my_spi.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Core/user/TFT/test.o: ../Core/user/TFT/test.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DSTM32F103xE -DUSE_HAL_DRIVER -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"D:/project/Personal_projects/heat_gun/firmware/demo/Core/user/TFT" -I"D:/project/Personal_projects/heat_gun/firmware/demo/Core/user/AT24C02" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/user/TFT/test.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

